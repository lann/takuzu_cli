#include "Takuzu_Solve.h"
#include "Board.h"
#include <cstddef>
#include <iostream>
Takuzu_Solve::Takuzu_Solve()
{
  
}

Takuzu_Solve::~Takuzu_Solve()
{

}

Takuzu_Solve::Takuzu_Solve(Board *board)
{
     m_board=board;
}

void Takuzu_Solve::Solve()
{
     bool finished=false;
     std::string out;
     while (!finished)
     {
	  finished=true;
	  for (size_t i=0; i<m_board->Get_size_row();i++)
	  {
	       for (size_t j=0; j<m_board->Get_size_column();j++)
	       {
		    for (size_t k=0;k<2;k++)
		    {
			 int return_find;
			 if (m_board->Get_Value(i, j)==-1)
			 {
			      return_find=Find_double_line(i, j, k);
			      if (return_find==n_right_or_down_direction)
			      {
				   if ( m_board->Set_Value(i, j, !k))
				   {
					m_board->Display();
					finished=false;
				   }
				   if (m_board->Set_Value(i, j+3, !k))
				   {
					m_board->Display();
					finished=false;
				   }
			      }
			      else if (return_find==n_left_or_up_direction)
			      {
				   if (m_board->Set_Value(i, j, !k))
				   {
					finished=false;
					m_board->Display();
				   }
				   if (m_board->Set_Value(i, j-3, !k))
				   {
					finished=false;
					m_board->Display();
				   }
			      }
			      return_find=Find_double_column(i, j, k);
			      if (return_find==n_right_or_down_direction)
			      {
				   if (m_board->Set_Value(i, j, !k))
				   {
					finished=false;
					m_board->Display();
				   }
				   if (m_board->Set_Value(i+3, j, !k))
				   {
					finished=false;
					m_board->Display();
				   }
			      }
			      else if (return_find==n_left_or_up_direction)
			      {
				   if (m_board->Set_Value(i, j, !k))
				   {
					m_board->Display();
					finished=false;
				   }
				   if ( m_board->Set_Value(i-3, j, !k))
				   {
					m_board->Display();
					finished=false;
				   }
			      }
			      if ((Find_side_line(i, j, k)) || (Find_side_column(i, j, k)))
			      {
				   if ( m_board->Set_Value(i, j, !k))
				   {
					finished=false;
					m_board->Display();
				   }
			      }
			      if (Try_complete_line(i, k))
			      {
				   for(size_t c=0;c<m_board->Get_size_column();c++)
				   {
					m_board->Set_Value(i, c, k);
				   }
				   m_board->Display();
			      }
			      if (Try_complete_column(j, k))
			      {
				   for(size_t r=0;r<m_board->Get_size_row();r++)
				   {
					m_board->Set_Value(r, j, k);
				   }
				   m_board->Display();
			      }
			 }
		    }
	       }
	  }
     }
}

int Takuzu_Solve::Find_double_line(size_t row, size_t column, int value)
{
     if ((m_board->Get_Value(row, column+1)==value) && (m_board->Get_Value(row, column+2)==value))
     {
	  return n_right_or_down_direction;
     }
     else if ((m_board->Get_Value(row, column-1)==value) && (m_board->Get_Value(row, column-2)==value))
     {
	  return n_left_or_up_direction;
     }
     else
     {
	  return false;
     }
}

int Takuzu_Solve::Find_double_column(size_t row, size_t column, int value)
{
     if ((m_board->Get_Value(row+1, column)==value) && (m_board->Get_Value(row+2, column)==value))
     {
	  return n_right_or_down_direction;
     }
     else if ((m_board->Get_Value(row-1, column)==value) && (m_board->Get_Value(row-2, column)==value))
     {
	  return n_left_or_up_direction;
     }
     else
     {
	  return false;
     }
}

bool Takuzu_Solve::Find_side_line(size_t row, size_t column, int value)
{
     if ((m_board->Get_Value(row+1, column)==value) && (m_board->Get_Value(row-1, column)==value))
     {
	  return true;
     }
     else
     {
	  return false;
     }
}

bool Takuzu_Solve::Find_side_column(size_t row, size_t column, int value)
{
     if ((m_board->Get_Value(row, column+1)==value) && (m_board->Get_Value(row, column-1)==value))
     {
	  return true;
     }
     else
     {
	  return false;
     }
}

bool Takuzu_Solve::Try_complete_line(size_t row, int value)
{
     bool ret=false;
     size_t quantity_inv_value(0), quantity_value(0);

     for (size_t i=0; i<m_board->Get_size_column();i++)
     {
	  if (m_board->Get_Value(row, i)==!value)
	  {
	       quantity_inv_value++;
	  }
	  if (m_board->Get_Value(row, i)==value)
	  {
	       quantity_value++;
	  }
     }
     if ((quantity_inv_value==m_board->Get_size_column()/2) && (quantity_value!=m_board->Get_size_column()/2))
     {
	  ret=true;
     }
     return ret;
}

bool Takuzu_Solve::Try_complete_column(size_t column,int value)
{
     bool ret=false;
     size_t quantity_inv_value(0), quantity_value(0);

     for (size_t i=0; i<m_board->Get_size_row();i++)
     {
	  if (m_board->Get_Value(i, column)==!value)
	  {
	       quantity_inv_value++;
	  }
	  if (m_board->Get_Value(i, column)==value)
	  {
	       quantity_value++;
	  }
     }
     if ((quantity_inv_value==m_board->Get_size_row()/2) && (quantity_value!=m_board->Get_size_row()/2))
     {
	  ret=true;
     }
     return ret;
}
