#include "Board.h"
#include <cstddef>
#include <ios>
#include <iostream>
#include <libintl.h>
#include <string>
#define _(String) gettext (String)
Board::Board()
{
  
}

Board::Board(size_t n_row, size_t n_col)
{
     m_row=n_row;
     m_column=n_col;
     Create();
}

Board::~Board() {}

void Board::Create(void)
{
     std::vector<int> vec(m_column,-1);
     for (size_t i=0; i<m_row;i++)
     {
	  m_vecboard.push_back(vec);
     }
}

bool Board::Create_from_file(std::string file)
{
     bool ret=true;
     file_csv.open(file,std::ios_base::in);
     if (file_csv.is_open())
     {
	  size_t lines(0),columns(0);
	  std::vector<std::string> vec_lines;
	  while (!file_csv.eof())
	  {
	       std::string line;
	       std::getline(file_csv,line);
	       vec_lines.push_back(line);
	       lines++;
	  }
	  if (vec_lines.size()>0)
	  {
	       for (size_t j=0; ((j<vec_lines.size()) && (ret==true)) ;j++)
	       {
		    size_t column(0);
		    std::string line_1;
		    column=Search_Columns(vec_lines.at(j));
		    if (j==0)
		    {
			 columns=column;
		    }
		    else if ((j!=0) && (column!=columns))
		    {
			 ret=false;
		    }
	       }
	  }
	  if (ret==true)
	  {
	       if ((columns%2!=0) || (lines%2!=0))
	       {
		    ret=false;
	       }
	       else
	       {
		    std::string line, caracter;
		    m_column=columns;
		    m_row=lines;
		    Create();
		    for (size_t i=0; i<vec_lines.size();i++)
		    {
			 size_t number_column(0);
			 line=vec_lines.at(i);
			 for (size_t j=0; j<line.size();j++)
			 {
			      caracter=line.at(j);
			      if (caracter!=";")
			      {
				   if (caracter=="0")
				   {
					Set_Value(i, number_column, 0);
				   }
				   else if (j!=0)
				   {
					std::string carac_minus;
					carac_minus=line.at(j-1);
					if ((carac_minus!="-") && (caracter=="1"))
					{
					     Set_Value(i, number_column, 1);
					}
				   }
				   else if ((j==0) && (caracter=="1"))
				   {
					Set_Value(i, number_column, 1);
				   }
			      }
			      else
			      {
				   number_column++;
			      }
			 }
		    }
	       }
	  }
     }
     else
     {
	  ret=false;
     }
     file_csv.close();
     return ret;
}

size_t Board::Search_Columns(std::string line)
{
     size_t ret(0);
     for(size_t i=0;i<line.size();i++)
     {
	  std::string carac;
	  carac=line.at(i);
	  if (carac==";")
	  {
	       ret++;
	  }
     }
     return ret;
}

void Board::Display()
{
     std::cout<<std::endl;
     for(size_t i=0;i<m_row;i++)
     {
	  std::cout<<"|";
	  for(size_t j=0;j<m_column;j++)
	  {
	       if (m_vecboard.at(i).at(j)!=-1)
	       {
		    std::cout<<m_vecboard.at(i).at(j)<<"|";
	       }
	       else
	       {
		    std::cout<<" |";
	       }
	  }
	  std::cout<<std::endl;
     }
}

void Board::Append()
{
     std::string out="";
     const std::string end=_("end");
     while (out!=end)
     {
	  int line=-1, col=-1, ele=-1;
	  while ((line<1) || (line>m_row))
	  {
	       std::cout<<_("Give the number of the line for the next element")<<std::endl;
	       std::cin>>line;
	  }
	  while ((col<1) || (col>m_column))
	  {
	       std::cout<<_("Give the number of the column for the next element")<<std::endl;
	       std::cin>>col;
	  }
	  while ((ele<0) || (ele>1))
	  {
	       std::cout<<_("Give the value of the element (0 or 1)")<<std::endl;
	       std::cin>>ele;
	  }
	  if (Validity_Value(line-1, col-1, ele))
	  {
	       m_vecboard.at(line-1).at(col-1)=ele;
	  }
	  else
	  {
	       std::cout<<_("The value ")<<ele<<_(" at the line ")<<line<<_(" and the column ")<<col<<_(" is not valid")<<std::endl;
	  }
	  Display();
	  std::cout<<_("For stop to append the board, type end or to continue, type c")<<std::endl;
	  std::cin>>out;
     }
}

bool Board::Validity_Value(size_t row, size_t column, int value)
{
     bool ret=true;
     if ((row>=0) && (row<m_row) && (column>=0) && (column<m_column))
     {
	  m_vecboard.at(row).at(column)=value;
	  size_t number_element=0;
	  int test_element=-1;
	  
	  for (size_t i=0;i<m_column && ret;i++)
	  {
	       if (test_element==-1)
	       {
		    test_element=m_vecboard.at(row).at(i);
		    number_element=1;
	       }
	       else if (test_element==m_vecboard.at(row).at(i))
	       {
		    number_element++;
	       }
	       else
	       {
		    test_element=m_vecboard.at(row).at(i);
		    number_element=1;
	       }
	       if (number_element>2)
	       {
		    ret=false;
	       }	       
	  }
	  test_element=-1;
	  number_element=0;
	  for (size_t i=0;i<m_row && ret;i++)
	  {
	       if (test_element==-1)
	       {
		    test_element=m_vecboard.at(i).at(column);
		    number_element=1;
	       }
	       else if (test_element==m_vecboard.at(i).at(column))
	       {
		    number_element++;
	       }
	       else
	       {
		    test_element=m_vecboard.at(i).at(column);
		    number_element=1;
	       }
	       if (number_element>2)
	       {
		    ret=false;
	       }	       
	  }
	  if (!ret)
	  {
	       m_vecboard.at(row).at(column)=-1;
	  }
     }
     else
     {
	  ret=false;
     }
     return ret;
}

bool Board::Set_Value(size_t row, size_t column, int value)
{
     bool ret=false;
     if ((row<m_row) && (column<m_column))
     {
	  if (m_vecboard.at(row).at(column)==-1)
	  {
	       if (Validity_Value(row, column, value))
	       {
		    m_vecboard.at(row).at(column)=value;
		    ret=true;
	       }
	  }
     }
     return ret;
}

int Board::Get_Value(size_t row, size_t column)
{
     if ((row<m_row) && (column<m_column))
     {
	  return m_vecboard.at(row).at(column);
     }
     else
     {
	  return -2;
     }
}

size_t Board::Get_size_column()
{
     return m_column;
}

size_t Board::Get_size_row()
{
     return m_row;
}

void Board::Set_size_row(size_t row)
{
     m_row=row;
}

void Board::Set_size_column(size_t column)
{
     m_column=column;
}
